using System.Collections.Generic;
using HarmonyLib;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace Th3Nametag;

public class Th3Nametag : ModSystem
{
    private Harmony harmony;

    private ICoreClientAPI _capi;

    public override void Start(ICoreAPI api)
    {
        harmony = new Harmony("Th3RP.patch");
        harmony.PatchAll();
    }

    public override void Dispose()
    {
        if (harmony != null)
        {
            harmony.UnpatchAll("Th3RP.patch");
        }
    }

    public override void StartClientSide(ICoreClientAPI api)
    {
        _capi = api;
        api.ChatCommands.GetOrCreate("nametag")
            .RequiresPrivilege(Privilege.commandplayer)
            .BeginSubCommand("check")
            .WithDescription("to check things with nametags and such")
            .HandleWith(Check)
            .EndSubCommand();
    }

    private TextCommandResult Check(TextCommandCallingArgs textCommandCallingArgs)
    {
        string msg = "";
        foreach (IPlayer player in _capi.World.AllPlayers)
        {
            bool maptag = player.Entity.WatchedAttributes.GetBool("maptag", true);
            int distance = player.Entity.WatchedAttributes.GetInt("nametagrenderrange", -1);
            msg += $"Client: {player.PlayerName} | nametag-distance: {distance} | maptag {maptag}\n";
        }

        return TextCommandResult.Success(msg);
    }

    public override void StartServerSide(ICoreServerAPI api)
    {
        api.ChatCommands.GetOrCreate("nametag")
            .RequiresPrivilege(Privilege.chat)
            .RequiresPlayer()
            .WithArgs(api.ChatCommands.Parsers.IntRange("nametag", 0, 99))
            .WithDescription(
                "Sets the nametag distance to the given integer, and if 0-20 (configurable number) turns off maptag. If 21-99, turns on maptag.")
            .HandleWith(OnDNDS);

        api.ChatCommands.GetOrCreate("maptag")
            .RequiresPrivilege(Privilege.chat)
            .RequiresPlayer()
            .WithDescription("Overrides map tag setting and turns it on")
            .WithArgs(api.ChatCommands.Parsers.Bool("nametag"))
            .HandleWith(OnDNDMS);
        api.Event.PlayerNowPlaying += PlayerNowPlaying;
    }

    private void PlayerNowPlaying(IServerPlayer byPlayer)
    {
        bool maptag = byPlayer.Entity.WatchedAttributes.GetBool("maptag", true);
        int distance = byPlayer.Entity.WatchedAttributes.GetInt("nametagrenderrange", 99);

        Mod.Logger.VerboseDebug($"{byPlayer.PlayerName} nametag-distance: {distance} | maptag {maptag}");

        var nameTagAttribs = byPlayer.Entity.WatchedAttributes.GetTreeAttribute("nametag");
        nameTagAttribs.SetInt("renderRange", distance);
        byPlayer.Entity.WatchedAttributes.MarkPathDirty("maptag");
        byPlayer.Entity.WatchedAttributes.MarkPathDirty("nametag");
    }

    private TextCommandResult OnDNDMS(TextCommandCallingArgs args)
    {
        var player = args.Caller.Player;
        var isVisible = (bool)args[0];
        player.Entity.WatchedAttributes.SetBool("maptag", isVisible);
        player.Entity.WatchedAttributes.MarkPathDirty("maptag");
        var msg = isVisible ? "on" : "off";
        return TextCommandResult.Success($"Your maptag is now set to {msg}, regardless of nametag display distance.");
    }

    private TextCommandResult OnDNDS(TextCommandCallingArgs args)
    {
        var player = args.Caller.Player;
        var distance = (int)args[0];

        distance = GameMath.Clamp(distance, 0, 99);
        var nameTagAttribs = player.Entity.WatchedAttributes.GetTreeAttribute("nametag");
        nameTagAttribs.SetInt("renderRange", distance);
        player.Entity.WatchedAttributes.SetInt("nametagrenderrange", distance);
        string msg;
        if (0 <= distance && distance <= 20)
        {
            player.Entity.WatchedAttributes.SetBool("maptag", false);
            msg = $"Nametag display distance now set to {distance}. Map tag turned off.";
        }
        else
        {
            player.Entity.WatchedAttributes.SetBool("maptag", true);
            msg = $"Nametag display distance now set to {distance}. Map tag turned on.";
        }

        player.Entity.WatchedAttributes.MarkPathDirty("nametag");
        player.Entity.WatchedAttributes.MarkPathDirty("maptag");
        player.Entity.WatchedAttributes.MarkPathDirty("nametagrenderrange");
        return TextCommandResult.Success(msg);
    }
}

[HarmonyPatch(typeof(PlayerMapLayer), "Render")]
public class Patch
{
    static AccessTools.FieldRef<PlayerMapLayer, Dictionary<IPlayer, EntityMapComponent>> MapCompsRef =
        AccessTools.FieldRefAccess<PlayerMapLayer, Dictionary<IPlayer, EntityMapComponent>>("MapComps");

    static bool Prefix(GuiElementMap mapElem, float dt, PlayerMapLayer __instance)
    {
        foreach (KeyValuePair<IPlayer, EntityMapComponent> val in MapCompsRef(__instance))
        {
            if (val.Value.entity.WatchedAttributes.GetBool("maptag", true))
            {
                val.Value.Render(mapElem, dt);
            }
        }

        return false;
    }
}